# Low-level process manipulation

This proposal attemts to define a portable and convenient way for Scheme programs to spawn and manipulate processes. The design should be such that higher-level libraries and interfaces to this library can be defined for use in modern implementations of projects such as `scsh`.

## Prior art

* JVM's [`java.lang.ProcessBuilder`](https://docs.oracle.com/javase/8/docs/api/java/lang/ProcessBuilder.html)
* Windows API's [`CreateProcessW`](https://learn.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createprocessw)
* POSIX's [`posix_spawn`](https://pubs.opengroup.org/onlinepubs/9699919799/functions/posix_spawn.html)

## Types

`process-data` shall be an opaque, disjoint and functional type for specifying the various attributes of the process to be spawned. An instance of the type shall be reusable to spawn multiple processes.

`process-handle` shall be an opaque, disjoint type which represents a spawned process.

## Procedures

### `process-data`

```scheme
(process-data? obj) => boolean
```

Returns whether `obj` is an instance of the `process-data` type.

```scheme
(process-data-create executable-path) => process-data
(process-data-create-from-path executable-name) => process-data
```

`process-data-create` and `process-data-create-from-path` are used to create the initial `process-data` state. `executable-path` and `executable-name` shall both be `string?` (TODO: How should we handle cases where the path or the executable name cannot be expressed in a string?). The difference between these procedures is that when the state returned from `process-data-create-from-path` is used to create a new process, the process executable is determined from the `executable-name` in an implementation-specific manner, usually by using the OS's ability to look up executables in the current process' path environment variable.

```scheme
(process-data-get-args process-data) => List of strings
(process-data-append-args process-data args ...) => process-data
```

`process-data-get-args` provides a copy of the currently specified arguments from `process-data` and gives them in a list from earliest to latest. `process-data-append-args` appends the provided strings and returns a new `process-data` where these arguments are added into the argument list.

**NOTE**: These arguments are all the arguments *after* the process name, which is always specified.

```scheme
(process-data-get-program-name process-data) => string
(process-data-with-program-name process-data name) => process-data
```

`process-data-get-program-name` provides the caller with the current process name, which by default corresponds to the `executable-name` or `executable-path` as provided in `process-data-create` and `process-data-create-from-path`. `process-data-with-program-name` allows the user to specify a custom name, and returns a corresponding `process-data`. Rationale is for applications which change their behaviour depending on the name they were invoked with.

```scheme
(process-data-get-working-directory process-data) => string
(process-data-with-working-directory process-data string) => process-data
```

`process-data-get-working-directory` gets the current working directory of the process to be spawned. By default this shall be the current working directory of the calling Scheme code. `process-data-with-working-directory` lets the user specify the working directory, and returns a corresponding `process-data`.

```scheme
(process-data-get-environment-variables process-data) => Alist of string to string
(process-data-with-environment-variables process-data env) => process-data
(process-data-add-environment-variables process-data env) => process-data
```

`process-data-get-environment-variables` gets a copy of the environment variables to be passed to the process in key-value pairs, which by default shall be the environment variables of the caller at the time when the `process-data` was created. `process-data-with-environment-variables` sets the passed alist `env` as the new environment variables, and `process-data-add-environment-variables` prepends to the existing ones. Once the process is created, the earlier key-value pair is used for a given environment variable.

```scheme
(process-data-with-standard-input-port process-data output-port) => process-data
(process-data-with-standard-output-port process-data input-port) => process-data
(process-data-with-standard-error-port process-data input-port) => process-data
```

These procedures allow setting the process' respective standard input, standard output and standard error to be the specified ports (TODO: Should these be specified to be textual ports or binary ports, or do we want to be able to use either?) By default the spawned process shall inherit the caller's current input, output and error ports.

```scheme
(process-data-spawn process-data) => process-handle
```

`process-data-spawn` uses the information in the passed in `process-data` to spawn a new process, and returns a handle to this process.

### `process-handle`

```scheme
(process-handle? obj) => boolean
```

Returns whether `obj` is an instance of the `process-handle` type.

```scheme
(process-handle-wait process-handle) => exit-status
(process-handle-wait/timeout process-handle time) => exit-status or #f
```

`process-handle-wait` shall either block until the process represented by the `process-handle` has exited, or shall immediately return the process' `exit-status` if the process has already ended by the time `process-handle-wait` has been called. `exit-status` shall be an implementation-specific type, although it will most likely be an non-negative exact integer on most platforms.

`process-handle-wait/timeout` shall be similar, except that it will unblock after the time specified in `time`, in which case the procedure shall return `#f` to indicate that the wait timed out. (TODO: Should `time` be SRFI 18 time, or some other kind of time type?)
