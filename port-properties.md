# Port properties

This proposal attempts to define a way for Scheme programs to be able to access various useful properties about ports. It also allows for custom ports to define their own properties.

## Procedures

```scheme
(port-property-ref port symbol) => Maybe object
```

This procedure shall return a property, denoted by `symbol`, for a given port `port`. For now this shall be an SFRFI 189 Maybe container.

TODO: Should there also be a way for applications to define and set custom properties for existing ports? Also, should certain properties be mutable?

## Standard properties

* `filename` This property shall reflect the path to the file to which the port refers to. It would usually be set by `call-with-input-file` or `call-with-output-file`, although an implementation is allowed to find the filename of a port in another way, when a port is created in an implementation-specific manner.  
  The filename should be a string and should reflect user expectations. As a non-normative example, while on a Linux system, `"/dev/stdin"` is a valid path for the standard input port, it is not what the user would expect and thus should probably be avoided for this property.

* `transcoder` This property shall give the underlying transcoder for textual ports. Usually this would be the `native-transcoder`.

## Extensions to othjer SRFIs/standards

SRFI 181 should be extended such that when a custom port is created, the user can either pass in the properties as they are, or a dispatch procedure that can return the values when the port is queried.
For example, a transcoded port can use the filename property of the underlying binary port as its own filename.
